package com.kgtech.data.feed

import com.kgtech.data.remote.MycellPocRestInterface
import javax.inject.Inject

class MycellRemoteDataSource @Inject constructor(private val restInterface: MycellPocRestInterface) {

    fun fetchRecentPlanList() = restInterface.fetchRecentPlanList()

}