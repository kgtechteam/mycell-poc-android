package com.kgtech.data.feed

import com.kgtech.common.Resource
import com.kgtech.model.Plan
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MycellRepository @Inject constructor(private val remoteDataSource: MycellRemoteDataSource) {

    fun fetchRecentPlanList(): Observable<Resource<List<Plan>>> {
        return remoteDataSource
            .fetchRecentPlanList()
            .map<Resource<List<Plan>>> {
                Resource.Success(it)
            }.onErrorReturn { throwable ->
                Resource.Error(throwable)
            }.subscribeOn(Schedulers.io())
    }
}