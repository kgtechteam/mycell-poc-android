package com.kgtech.data.remote

import com.kgtech.model.Plan
import io.reactivex.Observable
import retrofit2.http.GET

interface MycellPocRestInterface {

    @GET("plan/all")
    fun fetchRecentPlanList(): Observable<List<Plan>>


}