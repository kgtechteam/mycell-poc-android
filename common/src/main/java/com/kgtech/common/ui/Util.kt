package com.kgtech.common.ui

import com.kgtech.common.BuildConfig
import com.kgtech.model.PlanItem

class Util {

    companion object {

        fun CheckNewVersionAvailable(versionToCompare: String): Boolean {
            var isCurrentVersionOld = false
            val currentVersionArray = BuildConfig.VERSION_NAME.split(".")
            val versionToCompareArray = versionToCompare.split(".")

            for ((index, value) in currentVersionArray.withIndex()) {
                if (value < versionToCompareArray[index]) {
                    isCurrentVersionOld = true
                }
            }
            return isCurrentVersionOld
        }
    }
}