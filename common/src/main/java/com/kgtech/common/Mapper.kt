package com.kgtech.common

interface Mapper<R, D> {
    fun mapFrom(type: R): D
}