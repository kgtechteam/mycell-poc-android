package com.kgtech.domain

import com.kgtech.common.Resource
import com.kgtech.common.map
import com.kgtech.data.feed.MycellRepository
import com.kgtech.model.PlanItem
import io.reactivex.Observable
import javax.inject.Inject

class FetchRecentPlanUseCase @Inject constructor(
    private val repository: MycellRepository,
    private val mapper: FetchRecentPlanMapper
) {

    fun fetchRecentPlanList(): Observable<Resource<List<PlanItem>>> {
        return repository
            .fetchRecentPlanList()
            .map { resource ->
                resource.map { response ->
                    mapper.mapFrom(response)
                }
            }.startWith(Resource.Loading)
    }
}