package com.kgtech.domain

import com.kgtech.common.Mapper
import com.kgtech.model.Plan
import com.kgtech.model.PlanItem
import javax.inject.Inject

class FetchRecentPlanMapper @Inject constructor() : Mapper<List<Plan>, List<PlanItem>> {

    override fun mapFrom(type: List<Plan>): List<PlanItem> {
        return type.map { itemResponse ->
            PlanItem(
                created = itemResponse.created,
                deleted = itemResponse.deleted,
                name = itemResponse.name,
                oid = itemResponse.oid,
                planAttributes = itemResponse.planAttributes,
                price = itemResponse.price,
                updated = itemResponse.updated
            )
        }
    }
}
