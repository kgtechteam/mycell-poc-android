package com.kgtech.mycellpoc.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.kgtech.mycellpoc.R
import com.kgtech.mycellpoc.databinding.FragmentHomeBinding
import com.kgtech.mycellpoc.ui.base.BaseFragment

class HomeFragment : BaseFragment<FragmentHomeBinding, VMHomeFragment>(), HomeAdapter.ItemClickListener{

    override val viewModelClass: Class<VMHomeFragment> = VMHomeFragment::class.java
    override val layoutRes: Int = R.layout.fragment_home

    private lateinit var adapter: HomeAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel

        adapter = HomeAdapter(this)
        with(binding){
            recyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerview.adapter = adapter

        }

        return binding.root
    }

    override fun onItemClick(view: View, position: Int, title: String) {
        Toast.makeText(context, "$title clicked", Toast.LENGTH_SHORT).show()
    }

}
