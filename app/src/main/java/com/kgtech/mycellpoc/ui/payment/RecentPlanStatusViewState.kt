package com.kgtech.mycellpoc.ui.payment

import com.kgtech.common.Status


class RecentPlanStatusViewState(
    val status: Status
) {
    fun isLoading() = status is Status.Loading

    fun getErrorMessage() = if (status is Status.Error) status.exception.message else ""

    fun shouldShowErrorMessage() = status is Status.Error
}