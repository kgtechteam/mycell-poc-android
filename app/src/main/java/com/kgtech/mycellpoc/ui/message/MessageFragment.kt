package com.kgtech.mycellpoc.ui.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kgtech.mycellpoc.R
import com.kgtech.mycellpoc.databinding.FragmentMessageBinding
import com.kgtech.mycellpoc.ui.base.BaseFragment

class MessageFragment : BaseFragment<FragmentMessageBinding, VMMessageFragment>() {
    override val viewModelClass: Class<VMMessageFragment> = VMMessageFragment::class.java
    override val layoutRes: Int = R.layout.fragment_message

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel

        return binding.root
    }
}
