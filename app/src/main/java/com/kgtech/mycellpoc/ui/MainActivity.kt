package com.kgtech.mycellpoc.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import com.kgtech.common.setupWithNavController
import com.kgtech.mycellpoc.R
import com.kgtech.mycellpoc.databinding.ActivityMycellBinding
import com.kgtech.mycellpoc.ui.base.BaseActivity


class MainActivity : BaseActivity<ActivityMycellBinding, MainActivityViewModel>() {

    override val viewModelClass: Class<MainActivityViewModel> = MainActivityViewModel::class.java
    override val layoutRes: Int = R.layout.activity_mycell

    private var currentNavController: LiveData<NavController>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewModel = viewModel


        if (savedInstanceState == null) {
            setupBottomNavigationBar()
            binding.bottomNavigationBar.selectedItemId = R.id.nav_home
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        setupBottomNavigationBar()
    }

    private fun setupBottomNavigationBar() {

        val navGraphIds = listOf(
            R.navigation.nav_payment,
            R.navigation.nav_notifications,
            R.navigation.nav_home,
            R.navigation.nav_search,
            R.navigation.nav_message
        )

        val controller = binding.bottomNavigationBar.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.nav_host_container,
            intent = intent
        )

        controller.value?.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.nav_home) {

            }
        }
        currentNavController = controller
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController?.value?.navigateUp() ?: false
    }


    fun homeFabClickListener(view: View) {
        Toast.makeText(applicationContext, "Home", Toast.LENGTH_SHORT).show()
    }
}