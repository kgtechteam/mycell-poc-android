package com.kgtech.mycellpoc.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kgtech.mycellpoc.databinding.FragmentSearchBinding
import com.kgtech.mycellpoc.ui.base.BaseFragment

class SearchFragment : BaseFragment<FragmentSearchBinding, VMSearchFragment>() {

    override val viewModelClass: Class<VMSearchFragment> = VMSearchFragment::class.java
    override val layoutRes: Int = com.kgtech.mycellpoc.R.layout.fragment_search

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel


        return binding.root
    }

}
