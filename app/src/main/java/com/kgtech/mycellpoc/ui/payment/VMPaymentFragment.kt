package com.kgtech.mycellpoc.ui.payment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kgtech.common.*
import com.kgtech.domain.FetchRecentPlanUseCase
import com.kgtech.model.PlanItem
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import javax.inject.Inject

class VMPaymentFragment @Inject constructor(private val fetchRecentPlanUseCase: FetchRecentPlanUseCase):
    RxAwareViewModel() {


    private val contents = MutableLiveData<List<PlanItem>>()
    val contents_: LiveData<List<PlanItem>> = contents

    private val status = MutableLiveData<RecentPlanStatusViewState>()
    val status_: LiveData<RecentPlanStatusViewState> = status


    fun fetchRecentPlan() {
        fetchRecentPlanUseCase
            .fetchRecentPlanList()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { list ->
                Timber.d(list.toString())
                onRecentPhotoContentResultReady(list)

            }
            .subscribe({ resource ->
                Timber.d(resource.toString())
                onRecentPhotoStatusResultReady(resource)
            }, {})
            .also { disposable += it }
    }

    private fun onRecentPhotoStatusResultReady(resource: Resource<List<PlanItem>>) {

        val viewState = when (resource) {
            is Resource.Success -> RecentPlanStatusViewState(Status.Content)
            is Resource.Error -> RecentPlanStatusViewState(Status.Error(resource.exception))
            Resource.Loading -> RecentPlanStatusViewState(Status.Loading)
        }
        status.value = viewState
    }

    private fun onRecentPhotoContentResultReady(results: List<PlanItem>) {
        contents.value = results
    }
}