package com.kgtech.mycellpoc.ui.payment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.kgtech.common.observeNonNull
import com.kgtech.common.runIfNull
import com.kgtech.model.PlanItem
import com.kgtech.mycellpoc.R
import com.kgtech.mycellpoc.databinding.FragmentPaymentBinding
import com.kgtech.mycellpoc.ui.base.BaseFragment
import hari.bounceview.BounceView
import kotlinx.android.synthetic.main.toolbar_payment.*

class PaymentFragment : BaseFragment<FragmentPaymentBinding, VMPaymentFragment>() {

    override val viewModelClass: Class<VMPaymentFragment> = VMPaymentFragment::class.java
    override val layoutRes: Int = R.layout.fragment_payment

    private lateinit var adapter: PaymentAdapter
    private val gson = Gson()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        viewModel.contents_.observeNonNull(this) { contents ->
            renderLatestPlan(contents)
        }

        viewModel.status_.observeNonNull(this) { contents ->
            renderStatusResult(contents)
        }

        savedInstanceState.runIfNull {
            viewModel.fetchRecentPlan()
        }
        initRecyclerView()
        return binding.root
    }

    private fun renderStatusResult(statusViewState: RecentPlanStatusViewState) {
        binding.viewState = statusViewState
        binding.executePendingBindings()
    }
    private fun renderLatestPlan(contents: List<PlanItem>) {
        adapter.setPostList(contents)
    }
    private fun addAnim(v: View) {
        BounceView.addAnimTo(v).setScaleForPopOutAnim(1f, 1f)
    }

    private fun initRecyclerView(){
        adapter = PaymentAdapter()
        with(binding) {
            recyclerview.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerview.adapter = adapter

        }
    }

    //TODO animation eklenmeli
    private fun addButtonAnimations() {
        with(binding.paymentToolbar) {
            addAnim(button1)
            addAnim(button2)
            addAnim(button3)
            addAnim(button4)
            addAnim(button5)
        }
    }
}
