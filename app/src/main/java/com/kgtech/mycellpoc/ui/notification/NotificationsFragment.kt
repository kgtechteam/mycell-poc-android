package com.kgtech.mycellpoc.ui.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kgtech.mycellpoc.R
import com.kgtech.mycellpoc.databinding.FragmentNotificationsBinding
import com.kgtech.mycellpoc.ui.base.BaseFragment

class NotificationsFragment : BaseFragment<FragmentNotificationsBinding, VMNotificationsFragment>() {

    override val viewModelClass: Class<VMNotificationsFragment> = VMNotificationsFragment::class.java
    override val layoutRes: Int = R.layout.fragment_notifications

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel

        return binding.root
    }

}
