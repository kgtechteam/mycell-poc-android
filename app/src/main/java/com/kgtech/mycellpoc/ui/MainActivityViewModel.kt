package com.kgtech.mycellpoc.ui

import com.kgtech.common.RxAwareViewModel
import javax.inject.Inject

class MainActivityViewModel @Inject constructor() : RxAwareViewModel()
