package com.kgtech.mycellpoc.ui.payment

import com.kgtech.common.ui.Util
import com.kgtech.model.PlanItem


class RecentPlanItemViewState(private val planItem: PlanItem) {

    fun getTitle() = planItem.name
}