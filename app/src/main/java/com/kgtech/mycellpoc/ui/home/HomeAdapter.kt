package com.kgtech.mycellpoc.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.kgtech.mycellpoc.R
import hari.bounceview.BounceView
import kotlinx.android.synthetic.main.row_home_list.view.*

/**
 * Created by MuratCan on 2019-12-13.
 */

class HomeAdapter(val listener: ItemClickListener) : RecyclerView.Adapter<HomeAdapter.HomeAdapterViewHolder>() {

    var titles: MutableList<String> = mutableListOf("HAT SİPARİŞİM", "SIK SORULAN SORULAR", "MYCELL ASİSTAN")
    private var lastPosition = -1

    fun addPostList(items: List<String>) {
        val beforeSize = titles.size
        titles.addAll(items)
        notifyItemRangeInserted(beforeSize, titles.size)
    }

    fun setPostList(items: List<String>) {
        titles.clear()
        titles.addAll(items)
        notifyDataSetChanged()
    }

    fun removeItem(pos: Int) {
        titles.removeAt(pos)
        notifyItemRemoved(pos)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeAdapterViewHolder {
        return HomeAdapterViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_home_list, parent, false))
    }

    override fun getItemCount(): Int {
        return titles.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: HomeAdapterViewHolder, position: Int) {
        val mItem = titles[position]
        holder.setData(mItem)
        setAnimation(holder.itemView, position)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(viewToAnimate.context, android.R.anim.slide_in_left)
            animation.duration = 350
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    inner class HomeAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setData(item: String) {
            with(itemView) {
                title.text = item

                if (adapterPosition == titles.size-1)
                    bottom_line.visibility = View.GONE
                else
                    bottom_line.visibility = View.VISIBLE

                click.setOnClickListener {
                    listener.onItemClick(it, adapterPosition, item)
                }

                BounceView.addAnimTo(click).setScaleForPopOutAnim(1f, 1f)
            }
        }

    }

    interface ItemClickListener {
        fun onItemClick(view: View, position: Int, title: String)
    }

}