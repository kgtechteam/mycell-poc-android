package com.kgtech.mycellpoc.di.component

import android.app.Application
import com.kgtech.data.di.module.DataModule
import com.kgtech.mycellpoc.MycellApplication
import com.kgtech.mycellpoc.di.module.ActivityBuilderModule
import com.kgtech.mycellpoc.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilderModule::class,
        ViewModelModule::class,
        DataModule::class
    ]
)
interface AppComponent : AndroidInjector<MycellApplication> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Application): AndroidInjector<MycellApplication>
    }
}