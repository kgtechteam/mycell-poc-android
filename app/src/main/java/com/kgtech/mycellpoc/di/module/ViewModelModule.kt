package com.kgtech.mycellpoc.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kgtech.mycellpoc.di.ViewModelFactory
import com.kgtech.mycellpoc.di.key.ViewModelKey
import com.kgtech.mycellpoc.ui.MainActivityViewModel
import com.kgtech.mycellpoc.ui.home.VMHomeFragment
import com.kgtech.mycellpoc.ui.message.VMMessageFragment
import com.kgtech.mycellpoc.ui.notification.VMNotificationsFragment
import com.kgtech.mycellpoc.ui.payment.VMPaymentFragment
import com.kgtech.mycellpoc.ui.search.VMSearchFragment
import com.kgtech.mycellpoc.ui.splash.SplashActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @IntoMap
    @Binds
    @ViewModelKey(MainActivityViewModel::class)
    fun provideMainActivityViewModel(mainActivityViewModel: MainActivityViewModel): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(SplashActivityViewModel::class)
    fun provideSplashActivityViewModel(splashActivityViewModel: SplashActivityViewModel): ViewModel

    @Binds
    fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory


    @IntoMap
    @Binds
    @ViewModelKey(VMHomeFragment::class)
    abstract fun provideVMHomeFragment(HomeViewModel: VMHomeFragment): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMMessageFragment::class)
    abstract fun provideVMMessageFragment(MessageViewModel: VMMessageFragment): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMNotificationsFragment::class)
    abstract fun provideVMNotificationsFragment(NotificationsViewModel: VMNotificationsFragment): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMPaymentFragment::class)
    abstract fun provideVMPaymentFragment(PaymentViewModel: VMPaymentFragment): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMSearchFragment::class)
    abstract fun provideVMSearchFragment(SearchViewModel: VMSearchFragment): ViewModel
}