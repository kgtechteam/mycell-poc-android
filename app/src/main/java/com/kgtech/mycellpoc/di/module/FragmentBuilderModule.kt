package com.kgtech.mycellpoc.di.module

import com.kgtech.mycellpoc.di.scope.FragmentScope
import com.kgtech.mycellpoc.ui.home.HomeFragment
import com.kgtech.mycellpoc.ui.notification.NotificationsFragment
import com.kgtech.mycellpoc.ui.payment.PaymentFragment
import com.kgtech.mycellpoc.ui.message.MessageFragment
import com.kgtech.mycellpoc.ui.search.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface FragmentBuilderModule {



    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMessageFragment(): MessageFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeNotificationsFragment(): NotificationsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributePaymentFragment(): PaymentFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment


}