package com.kgtech.mycellpoc.di.module

import com.kgtech.mycellpoc.di.scope.ActivityScope
import com.kgtech.mycellpoc.ui.MainActivity
import com.kgtech.mycellpoc.ui.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [FragmentBuilderModule::class])
    fun bindMainActivity(): MainActivity


    @ActivityScope
    @ContributesAndroidInjector
    fun bindSplashActivity(): SplashActivity


}