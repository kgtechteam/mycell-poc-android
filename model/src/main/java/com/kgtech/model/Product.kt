package com.kgtech.model

data class Product(
    val product_apps: String,
    val product_call: String,
    val product_internet: String,
    val product_name: String,
    val product_price: String
)