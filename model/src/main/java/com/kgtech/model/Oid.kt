package com.kgtech.model


import com.google.gson.annotations.SerializedName

data class Oid(
    @SerializedName("counter")
    val counter: Int?,
    @SerializedName("date")
    val date: String?,
    @SerializedName("machineIdentifier")
    val machineIdentifier: Int?,
    @SerializedName("processIdentifier")
    val processIdentifier: Int?,
    @SerializedName("time")
    val time: Long?,
    @SerializedName("timeSecond")
    val timeSecond: Int?,
    @SerializedName("timestamp")
    val timestamp: Long?
)