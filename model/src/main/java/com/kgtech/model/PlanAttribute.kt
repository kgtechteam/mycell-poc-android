package com.kgtech.model


import com.google.gson.annotations.SerializedName

data class PlanAttribute(
    @SerializedName("code")
    val code: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("subtext")
    val subtext: String?,
    @SerializedName("value")
    val value: String?
)