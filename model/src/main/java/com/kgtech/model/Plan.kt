package com.kgtech.model


import com.google.gson.annotations.SerializedName

data class Plan(
    @SerializedName("created")
    val created: String?,
    @SerializedName("deleted")
    val deleted: Boolean?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("oid")
    val oid: Oid?,
    @SerializedName("planAttributes")
    val planAttributes: List<PlanAttribute?>?,
    @SerializedName("price")
    val price: Long?,
    @SerializedName("updated")
    val updated: String?
)