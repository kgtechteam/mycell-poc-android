package com.kgtech.model

import com.kgtech.model.Product

data class ProductsItem(
    val products: List<Product>
)