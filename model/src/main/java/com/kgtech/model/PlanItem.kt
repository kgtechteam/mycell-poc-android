package com.kgtech.model
data class PlanItem(
    val created: String?,
    val deleted: Boolean?,
    val name: String?,
    val oid: Oid?,
    val planAttributes: List<PlanAttribute?>?,
    val price: Long?,
    val updated: String?
)